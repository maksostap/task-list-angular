import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TasksService } from '../services/tasks.service';
import { Task } from '../task/task.component';
import {Location} from '@angular/common';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {
  //@Input() task:any;
  //task!:Task;
  task = new Task();
  taskForm: FormGroup;

  submitted = false;
  constructor(private taskService: TasksService, private route: ActivatedRoute, fb: FormBuilder, private location: Location) {
    this.taskForm = fb.group({
      'title': [this.task.title, [Validators.required,Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]],
      'content': [this.task.content],
    });
  }

  ngOnInit(): void {
    if (this.id) {
      this.getTask(parseInt(this.id));
    }
  }

  id: any = this.route.snapshot.paramMap.get('id');
  saveTask(): void {
    const data = {
      title: this.task.title,
      content: this.task.content
    };
    if (this.taskForm.valid) {
      if (this.id) {
        this.taskService.update(this.id, this.taskForm.getRawValue()).subscribe(() => {
          this.location.back();
        })
      }
      else {
        this.taskService.create(this.taskForm.getRawValue())
          .subscribe(
            () => {
              this.location.back();
            },
            error => {
              console.log(error);
            });
      }
    }
    else {
      console.log('Invalid form')
    }
  }

  getTask(id: number) {
    this.taskService.get(id).subscribe(resp => {
      this.taskForm.patchValue(resp);
    });
  }


}
