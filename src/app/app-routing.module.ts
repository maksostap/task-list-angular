import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskFormComponent } from './task-form/task-form.component';
import { TaskViewComponent } from './task-view/task-view.component';


const routes: Routes=[
  {path:'', component:TaskListComponent },
  {path:'task-form', component:TaskFormComponent },
  {path:'task-form/:id', component:TaskFormComponent },
  {path:'task-view/:id', component:TaskViewComponent },

]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
