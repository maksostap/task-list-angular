import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TasksService } from '../services/tasks.service';
import { Task } from '../task/task.component';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.css']
})
export class TaskViewComponent implements OnInit {

  constructor(private route:ActivatedRoute, private taskService: TasksService) { }
  id:any=this.route.snapshot.paramMap.get('id');
  task:Task=new Task();
  ngOnInit(): void {
    this.getTask();
  }
  getTask(){
    this.taskService.get(this.id).subscribe(resp=>{
      this.task=resp;
    });
  }
}
