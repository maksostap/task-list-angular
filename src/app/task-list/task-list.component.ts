import { Component, OnInit } from '@angular/core';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  tasks: Task[]=[];
  loaded:boolean=false;
  constructor(private taskService:TasksService) {
   
  }
  ngOnInit(): void {
    this.getTasks();
  }
  title = 'task-list-angular';

  getTasks(){
    this.taskService.getAll().subscribe(resp=>{
      this.tasks=resp;
      this.loaded=true;
    });
  }
}
