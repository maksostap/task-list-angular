import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TasksService } from '../services/tasks.service';
import { TaskListComponent } from '../task-list/task-list.component';

export class Task {

  constructor(public id: number = 0,
    public title: string = '',
    public content: string = '') {
    this.id = id;
    this.title = title;
    this.content = content;
  }
}

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() task: any;
  @Input() index: any;
  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  constructor(private taskService: TasksService, private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  view() {

  }

  delete() {
    this.taskService.delete(this.task.id).subscribe(resp => {
      this.refreshList.emit();
    })
  }

  openModal(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      if (result)
        this.delete();
    });
  }
}
